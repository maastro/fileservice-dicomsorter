@echo off
echo Starting ElasticSearch ...

SET JAVA_HOME="%~dp0\..\..\dependencies\zulu8.28.0.1-jdk8.0.163-win_x64"

call "%~dp0\elasticsearch-5.6.3\bin\elasticsearch.bat"
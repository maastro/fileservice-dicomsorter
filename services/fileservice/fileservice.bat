@echo off

SET APP_NAME=fileservice
TITLE %APP_NAME%
SET JAVA="%~dp0\..\..\dependencies\zulu8.28.0.1-jdk8.0.163-win_x64\bin\java.exe"

for %%f in (*.jar) do set JAR_FILE=%%~nf.jar --spring.profiles.active=standalone
%JAVA% -Xmx2048m -jar %JAR_FILE%

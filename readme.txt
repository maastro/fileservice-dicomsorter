** STARTUP **
Unzip elasticsearch version 5.X.X into FILESERVICE_HOME
Use startFileService.bat to start elasticSearch and the fileservice.
For ElasticSearch, make sure JAVA_HOME is set correctly (https://www.elastic.co/guide/en/elasticsearch/reference/2.4/_installation.html). 
Once both are running, http://localhost:8200/swagger-ui.html will allow access to fileservice functionality.

** DICOM STORING DATA **
By default, a DICOM STORESCP runs on port 104, with AE title FILESERVICE.
Use a DICOM toolkit such as dcmtk to perform a storescu of your data to the fileservice.
For example: storescu +sd +r localhost 104 -aec FILESERVICE "c:\Testdata\12345"
Once completed, your DICOM data will be indexed in elasticsearch.

** INDEXING DATA FROM DIRECTORY **
Use the dicom-index-controller at http://localhost:8200/swagger-ui.html to index all files for a given directory.
Once completed, your DICOM data will be indexed in elasticsearch.

** WRITING REFERENCED DATA TO FILE **
Once data is indexed, referencing RT-files (RTDOSE,RTPLAN,RTSTRUCT,CT) can be written to a configurable folder.
By default, data is written to {FILESERVICE_HOME}/dicomwrite.
Use the dicom-write-controller at http://localhost:8200/swagger-ui.html to write data for all patients or a specific patient id.
Data is structured to prevent data duplication as follows:
CT -> RTSTRUCTs referencing this CT -> RTPLANs referencing this RTSTRUCT -> RTDOSEs referencing this RTPLAN

** CONFIGURATION **
The application.yml can be used to configure the use of the default storescp (if turned off multiple other storescps can be started from the swagger-ui),
and to supply extra sorting parameters. for each modality, a dicomtag can be provided. The value of this dicomtag will determine the foldername for the file.
For example, setting the manufacturerModelName tag for CT, the structure might become: dicomwrite/12345/MAASTRO CLINIC/CT_1.2.3/...
@echo off

echo Downloading installation files for fileservice package...

SET ELASTICSEARCH_VERSION="5.6.3"
SET FILESERVICE_VERSION="1.0.33"
SET ZULU_JAVA_VERSION="8.28.0.1-jdk8.0.163-win_x64"


SET WGET_EXECUTABLE="%~dp0\..\utils\wget-1.19.4-win64\wget.exe"
SET ZIP_EXECUTABLE="%~dp0\..\utils\7-Zip\7z.exe"


SET JAVA_DESTINATION="%~dp0..\dependencies\zulu-java"
if not exist %JAVA_DESTINATION% (
    echo. & echo Downloading Java-%ZULU_JAVA_VERSION%...
    %WGET_EXECUTABLE% https://cdn.azul.com/zulu/bin/zulu%ZULU_JAVA_VERSION%.zip -O %JAVA_DESTINATION%.zip
    %ZIP_EXECUTABLE% x %JAVA_DESTINATION%.zip -o"%~dp0..\dependencies\"
    del "%JAVA_DESTINATION%.zip"
)

SET ELASTICSEARCH_DESTINATION="%~dp0..\services\elasticsearchservice\elasticsearch-%ELASTICSEARCH_VERSION%"
if not exist %ELASTICSEARCH_DESTINATION% (
    echo. & echo Downloading elasticsearch-%ELASTICSEARCH_VERSION%...
    %WGET_EXECUTABLE% https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-%ELASTICSEARCH_VERSION%.zip -O %ELASTICSEARCH_DESTINATION%.zip	
    %ZIP_EXECUTABLE% x "%ELASTICSEARCH_DESTINATION%.zip" -o"%~dp0..\services\elasticsearchservice\"
    del "%ELASTICSEARCH_DESTINATION%.zip"
)

SET FILESERVICE_DESTINATION="%~dp0..\services\fileservice\fileservice-%FILESERVICE_VERSION%.jar"
if not exist %FILESERVICE_DESTINATION% (
    echo. & echo Downloading fileservice...
    %WGET_EXECUTABLE% https://bitbucket.org/maastro/fileservice/downloads/fileservice-%FILESERVICE_VERSION%.jar -O %FILESERVICE_DESTINATION%
)


echo DONE!
echo.
pause


@echo off

set FILESERVICE_HOME="%~dp0..\services\fileservice\"

echo Starting MIA Services...
echo.
echo Starting ElasticSearch ...
start call "%~dp0..\services\elasticsearchservice\elasticsearch.bat"

CALL :waitForPort ElasticSearch , 9200

echo.
echo Starting FileService ...
cd %FILESERVICE_HOME%
start call fileservice.bat

CALL :waitForPort FileService , 8200
start "fileservice" http://localhost:8200/swagger-ui.html
EXIT /B %ERRORLEVEL%


:waitForPort
@echo off
echo. 
echo|set /p="Waiting for %~1 to register on port %~2 ..."
:checkPort
timeout 1 > nul
netstat -a -n -o | findstr :%2| findstr "LISTENING ABHÖREN" > nul
IF ERRORLEVEL 1 (
 echo|set /p="." 
 goto checkPort
)
echo  OK
EXIT /B 0

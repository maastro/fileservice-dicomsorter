# MIA FileService DICOM Sorter

The fileservice is a microservice used primarily in the Medical Image Analysis Framework. It indexes the DICOM metadata of inputfiles for advanced radiotherapy related DICOM querying.
Queries can be done using HTTP REST calls, documented by the swagger-ui (http://localhost:8200/swagger-ui.html by default). Input files can be provided either by indexing a folder structure or using a DICOM C-STORE. 
This repository is a preconfigured fileservice setup to sort DICOM files into radiotherapy packages based on their referenced UIDs.

## Getting started

### Installing and running microservices

1. Navigate to [downloads](https://bitbucket.org/maastro/fileservice-dicomsorter/downloads/)
2. Click on "Download repository"
3. Unzip
4. Download dependencies
	- Execute `./bin/download.bat`	
5. Start all services in once
	- Execute `./bin/start.bat`

### Indexing your DICOM files

#### Using the web API (recommended)

The web API can be used to index dicom files (the files are indexed, not copied). 

1. Use the "index-controller" [swagger-url](http://localhost:8200/swagger-ui.html#/index45controller)
2. Check the logfile for progress 
    
        \fileservice-package\services\fileservice\fileservice.log

#### Using storescu (e.g. dcmtk)

Use a DICOM toolkit such as dcmtk to perform a storescu of your data to the fileservice (the files are indexed and copied). Once completed, your DICOM data will be indexed in elasticsearch. 

Example: 
	
	storescu +sd +r localhost 104 -aec FILESERVICE "c:\Testdata\12345" 


### Sorting your DICOM files

Sorted data can be written to a folder. Files are sorted by their reference UIDs according to the following one-to-many relations:

CT >> RTSTRUCT >> RTPLAN >> RTDOSE
Data is structured to prevent data duplication as follows: CT -> RTSTRUCTs referencing this CT -> RTPLANs referencing this RTSTRUCT -> RTDOSEs referencing this RTPLAN

1. Make sure your DICOM data is indexed
2. Use the "dicom-write-controller" [swagger-url](http://localhost:8200/swagger-ui.html#/dicom45write45controller)
3. By default, data is written to 
	
		\fileservice-package\services\fileservice\data_write

## FileService components

The fileservice is a microservice (part of MIA framework) consisting of the following components:

- **[elasticsearchservice](https://www.elastic.co/):** ElasticSearch (search engine)
- **[fileservice](https://bitbucket.org/maastro/fileservice):** Java Spring Boot Microservice to search, sort and index dicom files


## Configuration

Configuration can be changed by editing the following yaml file (reboot required)

	\fileservice-package\services\fileservice\application-standalone.yml

WARNING tabs are not allowed in [yaml](https://en.wikipedia.org/wiki/YAML)!

### Sorting by other DICOM tags

For each modality a dicomtag can be provided. The value of this dicomtag will determine the foldername for the file. For example, setting the manufacturerModelName tag for CT, the structure might become: dicomwrite/12345/MAASTRO CLINIC/CT_1.2.3/...

References to lookup dicomtags:

- http://dicomlookup.com/lookuppage.asp
- https://sno.phy.queensu.ca/~phil/exiftool/TagNames/DICOM.html

EXAMPLE of sorting RTDOSES additional on Instance Number (0020,0013) and CTs on Manufacturer ModelName (0008,1090). 

application-standalone.yml

	filewriter:
	  outputFolder: dicomwrite
	  sorting:
	    RTDOSE: "00200013"
	    CT: "00081090"
		

### Exclude MR / PT (version 1.0.36+)

If you do not want to write MR or PT, you can exlcude writing them setting the following properties:

    dicom-write:
       excludeMR: false
       excludePT: false

### Out of memory

When receiving the following message in the console:

	java.lang.OutOfMemoryError: Java heap space

You can increase the max available memory by editing the following file "services/fileservice/fileservice.bat" (last line, -Xmx parameter)		

## Disclaimer

The MIA Fileservice does not come with warranties or guarantees of any kind. It has not been approved for clinical use. MAASTRO Clinic is not liable for any damages resulting from the use of this software.


## Contact

We are happy to help you with any questions. Feel free to contact us at [sdt@maastro.nl](mailto:sdt@maastro.nl).

